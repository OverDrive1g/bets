package com.example.bets.controller;

import com.example.bets.domain.Product;
import com.example.bets.domain.Views;
import com.example.bets.repo.ProductRepo;
import com.fasterxml.jackson.annotation.JsonView;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    public final ProductRepo productRepo;

    @Autowired
    public ProductController(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @GetMapping
    @JsonView(Views.IdName.class)
    public List<Product> list() {
        return productRepo.findAll();
    }

    @GetMapping("{id}")
    @JsonView(Views.FullProduct.class)
    public Product getOne(@PathVariable("id") ObjectId id) {
        return productRepo.findBy_id(id);
    }

    @PostMapping
    public Product create(@RequestBody Product product) {
        product.setCreatedDate(new Date(System.currentTimeMillis()));
        product.set_id(ObjectId.get());
        return productRepo.save(product);
    }

    @PutMapping("{id}")
    public Product update(
            @PathVariable("id") ObjectId id,
            @RequestBody Product product
    ){
        product.set_id(id);
        return productRepo.save(product);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") ObjectId id){
        productRepo.delete(productRepo.findBy_id(id));
    }
}
