package com.example.bets.domain;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Document(collection = "product")
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @JsonView(Views.Id.class)
    private ObjectId _id;
    @JsonView(Views.IdName.class)
    private String title;

    private String description;
    private float price;
    private List<Map<String, String>> attrs;
    private ObjectId owner_id;
    private Map<String, String> image;

    @DateTimeFormat(iso = ISO.DATE_TIME)
    @JsonView(Views.FullProduct.class)
    private Date createdDate;


    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<Map<String, String>> getAttrs() {
        return attrs;
    }

    public void setAttrs(List<Map<String, String>> attrs) {
        this.attrs = attrs;
    }

    public ObjectId getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(ObjectId owner_id) {
        this.owner_id = owner_id;
    }

    public Map<String, String> getImage() {
        return image;
    }

    public void setImage(Map<String, String> image) {
        this.image = image;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
