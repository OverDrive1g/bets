package com.example.bets.repo;

import com.example.bets.domain.Product;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepo extends MongoRepository<Product, String> {
    Product findBy_id(ObjectId _id);
}
