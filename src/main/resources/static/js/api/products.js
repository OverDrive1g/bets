import Vue from 'vue'

const products = Vue.resource('/product{/id}')

export default {
    add: product => products.save({}, product),
    update: product => products.update({id: product._id}, product),
    remove: id => products.remove({id:id}),
}