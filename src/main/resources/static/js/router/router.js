import Vue from 'vue'
import VueRouter from 'vue-router'

import ProductList from 'pages/ProductList.vue'
import ProductForm from 'pages/ProductForm.vue'
import Auth from 'pages/Auth.vue'
import Profile from 'pages/Profile.vue'
import Product from 'pages/Product.vue'

Vue.use(VueRouter)

const routes = [
    {path:'/', component: ProductList},
    {path:'/auth', component: Auth},
    {path:'/profile', component: Profile},
    {path:'/product_/:id', component: Product},
    {path:'/new-product', component: ProductForm},
    {path:'*', component: ProductList}
];

export default new VueRouter({
    mode: 'history',
    routes
})