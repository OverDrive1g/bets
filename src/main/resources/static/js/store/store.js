import Vue from 'vue'
import Vuex from 'vuex'

import productApi from 'api/products'


Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        products: frontendData.products,
        profile: frontendData.profile
    },
    getters:{
        sortedProducts: state => state.products.sort((a,b) => a.name > b.name),
        getProductById: state => id => state.products.find(i => i._id === id)
    },
    mutations:{
        addMessageMutation(state, product) {
            state.products = [
                ...state.products,
                product
            ]
        },
        updateMessageMutation(state, product) {
            const updateIndex = state.products.findIndex(item => item._id === product._id)
            state.products = [
                ...state.products.slice(0,updateIndex),
                product,
                ...state.products.slice(updateIndex + 1)
            ]
        },
        removeMessageMutation(state, product) {
            const deleteIndex = state.products.findIndex(item => item._id === product._id)

            if(deleteIndex > -1){
                state.products = [
                    ...state.products.slice(0, deleteIndex),
                    ...state.products.slice(deleteIndex + 1)
                ]
            }
        }
    },
    actions:{
        async addProductAction ({commit, state}, product){
            const result = await productApi.add(product)
            const data = await result.json()
            const index = state.products.findIndex(item => item._id === data._id)

            if(index > -1){
                commit('updateMessageMutation', data)
            } else {
                commit('addMessageMutation',data)
            }
        },
        async updateProductAction ({commit}, product){
            const result = await productApi.update(product)
            const data = await result.json()

            commit('updateMessageMutation', data)


        },
        async removeProductAction ({commit}, product){
            const result = await productApi.remove(product._id)

            if(result.ok) {
                commit('removeMessageMutation',product)
            }
        }
    }
})